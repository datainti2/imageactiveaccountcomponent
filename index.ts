import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '@angular/material';
import { BaseWidgetImageComponent } from './src/base-widget/base-widget.component';

export * from './src/base-widget/base-widget.component';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule
  ],
  declarations: [
    BaseWidgetImageComponent
  ],
  exports: [
  BaseWidgetImageComponent
  ]
})
export class ImageActiveAccountModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: ImageActiveAccountModule,
      providers: []
    };
  }
}
