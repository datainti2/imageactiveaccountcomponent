import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseWidgetImageComponent } from './base-widget.component';

describe('BaseWidgetImageComponent', () => {
  let component: BaseWidgetImageComponent;
  let fixture: ComponentFixture<BaseWidgetImageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BaseWidgetImageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseWidgetImageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
